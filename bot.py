import logging
import os
from uuid import uuid4

from telegram import (InlineQueryResultArticle, InputTextMessageContent)
from telegram.ext import (Updater, InlineQueryHandler, CommandHandler, MessageHandler, Filters)

from models import *

# Enable logging
logging.basicConfig(format='%(asctime)s - %(name)s - %(levelname)s - %(message)s', level=logging.INFO)
logger = logging.getLogger(__name__)


def load_environment():
    from dotenv import load_dotenv
    load_dotenv()

    # OR, the same with increased verbosity:
    load_dotenv(verbose=True)

    # OR, explicitly providing path to '.env'
    from pathlib import Path  # python3 only
    env_path = Path('.') / '.env'
    load_dotenv(dotenv_path=env_path)


load_environment()

database.init(os.getenv("DATABASE"))
database.connect()
database.create_tables([
    User,
    Message,
])


def save_data(update, context):
    chat_id = update.message.from_user.id
    username = update.message.from_user.username
    first_name = update.message.from_user.first_name
    last_name = update.message.from_user.last_name
    is_bot = update.message.from_user.is_bot
    language_code = update.message.from_user.language_code
    text = update.message.text
    details = str(update)

    user = User.select().where(User.chat_id == chat_id)

    if not len(user):
        User.insert({
            User.first_name: first_name,
            User.last_name: last_name,
            User.username: username,
            User.chat_id: chat_id,
            User.is_bot: is_bot,
            User.language_code: language_code,
        }).execute()
    if len(user):
        User.update({
            User.first_name: first_name,
            User.last_name: last_name,
            User.username: username,
            User.chat_id: chat_id,
            User.is_bot: is_bot,
            User.language_code: language_code,
        }).where(User.chat_id == chat_id).execute()

    Message.insert({
        Message.chat_id: chat_id,
        Message.username: username,
        Message.text: text,
        Message.details: details,
    }).execute()


# Define a few command handlers. These usually take the two arguments update and
# context. Error handlers also receive the raised TelegramError object in error.
def start(update, context):
    """Send a message when the command /start is issued."""
    update.message.reply_text('Hi!')


def help(update, context):
    """Send a message when the command /help is issued."""
    update.message.reply_text('I will Help you =D')


def inlinequery(update, context):
    """Handle the inline query."""
    query = update.inline_query.query

    result = 'Oops Something went wrong!'
    try:
        result = str(str(query) + ' = ' + str(eval(query)))
    except:
        pass

    results = []

    if eval(query) != None and len(query):
        results.append(
            InlineQueryResultArticle(
                id=uuid4(),
                title="Result",
                input_message_content=InputTextMessageContent(result),
                description='I will send the result for you',
            )
        )

    update.inline_query.answer(results)


def error(update, context):
    """Log Errors caused by Updates."""
    logger.warning('Update "%s" caused error "%s"', update, context.error)


def main():
    # Create the Updater and pass it your bot's token.
    # Make sure to set use_context=True to use the new context based callbacks
    # Post version 12 this will no longer be necessary
    updater = Updater(os.getenv("TOKEN"), use_context=True)

    # Get the dispatcher to register handlers
    bot = updater.dispatcher

    # on different commands - answer in Telegram
    bot.add_handler(MessageHandler(Filters.all, save_data), group=0)
    bot.add_handler(CommandHandler('start', start))
    bot.add_handler(CommandHandler('help', help))

    # on noncommand i.e message - echo the message on Telegram
    bot.add_handler(InlineQueryHandler(inlinequery))

    # log all errors
    bot.add_error_handler(error)

    # Start the Bot
    updater.start_polling()

    # Block until the user presses Ctrl-C or the process receives SIGINT,
    # SIGTERM or SIGABRT. This should be used most of the time, since
    # start_polling() is non-blocking and will stop the bot gracefully.
    updater.idle()


if __name__ == '__main__':
    main()
